package com.crossover.techtrial.controller;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;


/**
 * PanelControllerTest class will test all APIs in PanelController.java.
 * @author Crossover
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PanelControllerTest {
  
  MockMvc mockMvc;
  
  @Mock
  private PanelController panelController;
  
  @Autowired
  private TestRestTemplate template;

  @Before
  public void setup() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(panelController).build();
  }

  @Test
  public void testPanelShouldRejectedDueToSerialLengthFailure() throws Exception {
    HttpEntity<Object> panel = getHttpEntity(
        "{\"serial\": \"232323\", \"longitude\": \"54.123232\"," 
            + " \"latitude\": \"54.123232\",\"brand\":\"tesla\" }");
    ResponseEntity<Panel> response = template.postForEntity(
        "/api/register", panel, Panel.class);
    Assert.assertEquals(400,response.getStatusCode().value());
  }
  
  @Test
  public void testPanelShouldBeRegistered() throws Exception {
    HttpEntity<Object> panel = getHttpEntity(
        "{\"serial\": \"3232345697412354\", \"longitude\": \"17.1233132\"," 
            + " \"latitude\": \"43.31122\",\"brand\":\"tesla\" }");
    ResponseEntity<Panel> response = template.postForEntity(
        "/api/register", panel, Panel.class);
    Assert.assertEquals(202,response.getStatusCode().value());
  }
  
  @Test
  public void testSaveHourlyElectricityShouldBeSuccess() throws Exception {
    HttpEntity<Object> hourlyElectricity = getHttpEntity(
        "{\"panel_id\": \"1\", \"generated_electricity\": \"95\"," 
            + " \"reading_at\": \"'2018-02-1 10:30:00'\" }");
    ResponseEntity<HourlyElectricity> response = template.postForEntity("/api/panels/1234567890123456/hourly", hourlyElectricity, HourlyElectricity.class);
    System.out.println(response.getBody());
    Assert.assertEquals(200,response.getStatusCode().value());
  }
  
  @Test
  public void testAllHourlyElectricityShoudBeSuccess() throws Exception {
    ResponseEntity<?> response = template.getForEntity("/api/panels/1234567890123456/hourly", Object.class);
    Assert.assertEquals(200,response.getStatusCode().value());
  }
  
  @Test
  public void testAllHourlyElectricityShoudFailDueToInvalidSerial() throws Exception {
    ResponseEntity<Page> response = template.getForEntity("/api/panels/1111111111111111/hourly", Page.class);
    Assert.assertEquals(404,response.getStatusCode().value());
  }
  
  @Test
  public void testAllDailyElectricityTillYesterdayLengthShouldBeGreaterThanZero() throws Exception {
    ResponseEntity<List> response = template.getForEntity("/api/panels/1234567890123456/daily", List.class);
    Assert.assertTrue("Response body is Empty", response.getBody().size() > 0);
    Assert.assertEquals(200,response.getStatusCode().value());
  }
  
  @Test
  public void testAllDailyElectricityTillYesterdayLengthShouldBeGreaterThanZeroSerial2() throws Exception {
    ResponseEntity<List> response = template.getForEntity("/api/panels/2222222222222222/daily", List.class);
    System.out.println(response.getBody());
    Assert.assertTrue("Response body is Empty", response.getBody().size() > 0);
    Assert.assertEquals(200,response.getStatusCode().value());
  }
  
  @Test
  public void testAllDailyElectricityTillYesterdayShouldFailForInvalidSerial() throws Exception {
    ResponseEntity<List> response = template.getForEntity("/api/panels/1111111111111111/daily", List.class);
    Assert.assertEquals(404,response.getStatusCode().value());
  }

  private HttpEntity<Object> getHttpEntity(Object body) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity<Object>(body, headers);
  }
}
